const chart = "chart"
const parent = "parent"
const amount = 4
let objects = []

for (let i = 1; i <= amount; i++) {
    objects.push({
        parent: document.getElementById(parent + i),
        chart: document.getElementById(chart + i),
        ctx: document.getElementById(chart + i)
    })
}

addEventListener("resize", () => {
    for (let id in Chart.instances) {
        Chart.instances[id].destroy()
    }
    Chart.instances = {};
    objects = []
    setup()
})

objects.forEach(o => {
    o.char = new Chart(o.ctx, {
        type: 'line',
        data: {
            labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
            datasets: [{
                data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
                label: "Lundi",
                borderColor: "#3e95cd",
                fill: false
            }, {
                data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
                label: "Mardi",
                borderColor: "#8e5ea2",
                fill: false
            }, {
                data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
                label: "Mercredi",
                borderColor: "#3cba9f",
                fill: false
            }, {
                data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
                label: "Jeudi",
                borderColor: "#e8c3b9",
                fill: false
            }, {
                data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
                label: "Vendredi",
                borderColor: "#c45850",
                fill: false
            },           
            {
                data: [6, 3, 2, 2, 7, 60, 82, 172, 90, 433],
                label: "Samedi",
                borderColor: "#c56750",
                fill: false
            },
            {
                data: [6, 3, 2, 2, 7, 26, 82, 78, 312, 433],
                label: "Dimanche",
                borderColor: "#000000",
                fill: false
            }]
        },
        options: {
            title: {
                display: true,
                text: 'World population per region (in millions)'
            }
        }
    });
})

const setup = () => {
    for (let i = 1; i <= amount; i++) {
        objects.push({
            parent: document.getElementById(parent + i),
            chart: document.getElementById(chart + i),
            ctx: document.getElementById(chart + i)
        })
    }

    objects.forEach(o => {
        o.char = new Chart(o.ctx, {
            type: 'line',
            data: {
                labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
                datasets: [{
                    data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
                    label: "Africa",
                    borderColor: "#3e95cd",
                    fill: false
                }, {
                    data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
                    label: "Asia",
                    borderColor: "#8e5ea2",
                    fill: false
                }, {
                    data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
                    label: "Europe",
                    borderColor: "#3cba9f",
                    fill: false
                }, {
                    data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
                    label: "Latin America",
                    borderColor: "#e8c3b9",
                    fill: false
                }, {
                    data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
                    label: "North America",
                    borderColor: "#c45850",
                    fill: false
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'World population per region (in millions)'
                }
            }
        });
    })
}